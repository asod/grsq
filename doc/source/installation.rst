============
Installation
============
``grsq`` is available via pip_ ::

    $ pip install grsq

Which will install ``grsq`` and its dependencies. 

.. _pip: https://pip.pypa.io/en/stable/


Requirements
============
* Python_ 3.7 or newer
* NumPy_ 
* SciPy_ 
* ASE_ 
* periodictable_ for atomic form factors
* MDAnalysis_ for automatic stoichometries from topology-files

.. _Python: https://www.python.org/
.. _NumPy: https://docs.scipy.org/doc/numpy/reference/
.. _SciPy: https://docs.scipy.org/doc/scipy/reference/
.. _periodictable: https://pypi.org/project/periodictable/
.. _ASE: https://wiki.fysik.dtu.dk/ase
.. _MDAnalysis: https://www.mdanalysis.org/

